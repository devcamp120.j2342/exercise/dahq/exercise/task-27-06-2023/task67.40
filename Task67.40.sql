-- cau1 
-- cau2 Viết câu lệnh SQL liệt kê số lượng khách hàng ở mỗi quốc gia
SELECT country, COUNT(*) as customer_count
FROM customers
GROUP BY country;
-- cau3 Viết câu lệnh SQL liệt kê số lượng khách hàng ở mỗi thành phố.
SELECT city, COUNT(*) as customer_count
FROM customers
GROUP BY city;
-- cau4 Viết câu lệnh SQL liệt kê số lượng khách hàng ở mỗi quốc gia, được sắp xếp từ cao đến thấp
SELECT country, COUNT(*) as customer_count
FROM customers
GROUP BY country
ORDER BY customer_count DESC;
-- cau5 Viết câu lệnh SQL liệt kê số lượng khách hàng ở mỗi thành phố, được sắp xếp từ cao đến thấp
SELECT city, COUNT(*) as customer_count
FROM customers
GROUP BY city
ORDER BY customer_count DESC;
